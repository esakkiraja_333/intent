package com.ahaguru.sampleprojectintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ActivityB extends AppCompatActivity {

TextView email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_b);
        email = findViewById(R.id.tvEmail);
        Intent i = getIntent();
        String emaily = i.getStringExtra("email");
        email.setText(" "+emaily);
    }
}