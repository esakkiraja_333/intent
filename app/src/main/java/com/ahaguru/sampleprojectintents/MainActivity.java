package com.ahaguru.sampleprojectintents;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    Button implicit;
    EditText email,password;
    String emailString,passString;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        implicit = findViewById(R.id.btnImplicit);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);

        implicit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                emailString = email.getText().toString();
                passString = password.getText().toString();
                Intent intent = new Intent(getApplicationContext(),ActivityB.class);
                intent.putExtra("email",""+emailString);
                startActivity(intent);
            }
        });

    }
}